//
//  AnotherViewController.swift
//  Chumthea-HomeWork-003
//
//  Created by MacBook on 12/1/19.
//  Copyright © 2019 MacBook. All rights reserved.
//

import UIKit

class AnotherViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .white
    }
    
}
