//
//  ContentWithImageTableViewCell.swift
//  Chumthea-HomeWork-003
//
//  Created by MacBook on 12/1/19.
//  Copyright © 2019 MacBook. All rights reserved.
//

import UIKit

class ContentWithImageTableViewCell: UITableViewCell {

    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userSubLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var captionLabel: UILabel!
    @IBOutlet weak var postPhotoImageView: UIImageView!
    @IBOutlet weak var postByPhotoImageView: UIImageView!
    @IBOutlet weak var postByDownButton: UIButton!
    @IBOutlet weak var numberOfCommentsLabel: UILabel!
    @IBOutlet weak var numberOfLikesLabel: UILabel!
    @IBOutlet weak var commentTextField: UITextField!
    @IBOutlet weak var ellipseButton: UIButton!
    
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var bottomView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        userNameLabel.font = UIFont.preferredFont(forTextStyle: .headline)
        customizeCommentTextField(commentTextField)
        
        customizeProfileImageView(profileImageView)
        customizeProfileImageView(postByPhotoImageView)
        
        customizeSocialButton(shareButton)
        customizeSocialButton(likeButton)
        customizeSocialButton(postByDownButton)
        customizeSocialButton(ellipseButton)
        
        customizeSubLabel(numberOfLikesLabel)
        customizeSubLabel(numberOfCommentsLabel)
        customizeSubLabel(userSubLabel)
        
        customizeCaptionLabel()
        
        postPhotoImageView.contentMode = .scaleAspectFill
        
        bottomView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.3)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func customizeCommentTextField(_ textField : UITextField){
       textField.layer.cornerRadius = 15
       textField.layer.borderWidth = 1
       textField.borderStyle = .none
       textField.layer.borderColor = UIColor.clear.cgColor
       textField.layer.backgroundColor = UIColor.lightGray.withAlphaComponent(0.2).cgColor
       textField.placeholder = "Write your comment..."
       textField.textColor = .black
       
       
       textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: textField.frame.height))
       textField.leftViewMode = .always
       
       textField.tintColor = .white
     }
    
    func customizeProfileImageView(_ imageView : UIImageView){
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = imageView.frame.size.width / 2
        imageView.contentMode = .scaleAspectFill
    }
    
    func customizeSocialButton(_ button: UIButton){
        button.tintColor = .black
    }
    
    func customizeSubLabel(_ label : UILabel){
        label.textColor = .darkGray
        label.font = UIFont.preferredFont(forTextStyle: .subheadline)
    }
    
    func customizeCaptionLabel(){
        captionLabel.adjustsFontSizeToFitWidth = true
        captionLabel.numberOfLines = 3
        captionLabel.minimumScaleFactor = 0.2
    }
}

