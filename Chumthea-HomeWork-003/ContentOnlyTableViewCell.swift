//
//  ContentOnlyTableViewCell.swift
//  Chumthea-HomeWork-003
//
//  Created by MacBook on 12/1/19.
//  Copyright © 2019 MacBook. All rights reserved.
//

import UIKit

class ContentOnlyTableViewCell: UITableViewCell {

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userNameSubLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var commentTextField: UITextField!
    @IBOutlet weak var numberOfLikesLabel: UILabel!
    
    @IBOutlet weak var numberOfCommentsLabel: UILabel!
    
    @IBOutlet weak var postByImageView: UIImageView!
    @IBOutlet weak var postByDownButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var ellipseButton: UIButton!
    
    @IBOutlet weak var bottomView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        customizeProfileImageView(profileImageView)
        customizeProfileImageView(postByImageView)
        
        userNameLabel.font = UIFont.preferredFont(forTextStyle: .headline)
        customizeUserNameSubLabel()
        customizeContentLabel()
        
        customizeCommentTextField(commentTextField)
        
        customizeSocialButton(shareButton)
        customizeSocialButton(likeButton)
        customizeSocialButton(postByDownButton)
        customizeSocialButton(ellipseButton)
        
        customizeSubLabel(userNameSubLabel)
        customizeSubLabel(numberOfCommentsLabel)
        customizeSubLabel(numberOfLikesLabel)
        bottomView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.3)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func customizeCommentTextField(_ textField : UITextField){
      textField.layer.cornerRadius = 15
      textField.layer.borderWidth = 1
      textField.borderStyle = .none
      textField.layer.borderColor = UIColor.clear.cgColor
      textField.layer.backgroundColor = UIColor.lightGray.withAlphaComponent(0.2).cgColor
      textField.placeholder = "Write your comment..."
      textField.textColor = .black
      
      
      textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: commentTextField.frame.height))
      textField.leftViewMode = .always
      
      textField.tintColor = .black
    }
    
    func customizeProfileImageView(_ imageView : UIImageView){
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = imageView.frame.size.width / 2
        imageView.contentMode = .scaleAspectFill
    }
    
    func customizeUserNameSubLabel(){
        userNameSubLabel.font = UIFont.preferredFont(forTextStyle: .subheadline)
        userNameSubLabel.textColor = .darkGray
    }
    
    func customizeContentLabel(){
        contentLabel.adjustsFontSizeToFitWidth = true
        contentLabel.numberOfLines = 3
        contentLabel.minimumScaleFactor = 0.7
//        contentLabel.font = UIFont.preferredFont(forTextStyle: .body)
    }
    
    
    func customizeSocialButton(_ button: UIButton){
        button.tintColor = .black
    }
    
    func customizeSubLabel(_ label : UILabel){
        label.textColor = .darkGray
        label.font = UIFont.preferredFont(forTextStyle: .subheadline)
    }
}
