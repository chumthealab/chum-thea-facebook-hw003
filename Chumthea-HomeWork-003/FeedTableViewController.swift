//
//  FeedTableViewController.swift
//  Chumthea-HomeWork-003
//
//  Created by MacBook on 12/1/19.
//  Copyright © 2019 MacBook. All rights reserved.
//

import UIKit

class FeedTableViewController: UITableViewController {

    
    var postDatas = [
        PostModel(userName: "Dong Shancai", photoUrl: "shenyue", postDuration: 0, location: "Bejing, China", caption: "Whenever those days come, you tell me How precious I am Everything’s alright From nobody to somebody I become a very special me", postPhotoUrl: nil, numberOfLike: 1, numberOfComment: 100),
        PostModel(userName: "Sana", photoUrl: "shenyue1", postDuration: 100, location: "Phnom Penh", caption: "You make me feel special.", postPhotoUrl: nil, numberOfLike: 100, numberOfComment: 100),
        PostModel(userName: "Hitomi Honda", photoUrl: "shenyue2", postDuration: 10, location: "Kyouto, Japan", caption: "You make me feel special No matter how the world brings me down Even when hurtful words stab me I smile again that’s what you do.", postPhotoUrl: "shenyue1", numberOfLike: 8808, numberOfComment: 100),
        PostModel(userName: "Shen Yue", photoUrl: "shenyue3", postDuration: 286900, location: "Guangdong, China", caption: "Again I feel special One moment I feel like I’m nothing at all Like no one would notice if I were gone But then when I hear you calling me I feel loved, I feel so special.", postPhotoUrl: nil, numberOfLike: 1000, numberOfComment: 1),
        PostModel(userName: "Mina", photoUrl: "shancai3", postDuration: 100, location: "Tokyo, Japan", caption: "no one would notice if I were gone But then when I hear you calling me I feel loved, I feel so special", postPhotoUrl: "shancai1", numberOfLike: 100, numberOfComment: 100028982),
        PostModel(userName: "Sakura Miyami", photoUrl: "shenyue4", postDuration: 10, location: "Seoul, South Korea", caption: "I love myself.", postPhotoUrl: "shancai2", numberOfLike: 1000222, numberOfComment: 1100),

    ]

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.lightText.withAlphaComponent(0.9)
        
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
        tableView.register(UINib(nibName: "ContentOnlyTableViewCell", bundle: nil), forCellReuseIdentifier: "ContentOnlyReuseIdentifier")
        tableView.register(UINib(nibName: "ContentWithImageTableViewCell", bundle: nil), forCellReuseIdentifier: "ContentWithImageReuseIdentifier")
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return postDatas.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let post = postDatas[indexPath.row]
        if post.postPhotoUrl == nil {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ContentOnlyReuseIdentifier", for: indexPath) as! ContentOnlyTableViewCell
            cell.userNameSubLabel.text = "\(post.postDuration.abbreviationSeconds()) . \(post.location)"
            cell.userNameLabel.text = post.userName
            cell.profileImageView.image = UIImage(named: post.photoUrl)
            cell.contentLabel.text = post.caption
            cell.postByImageView.image = UIImage(named: post.photoUrl)
            
            cell.numberOfLikesLabel.text = "\(post.numberOfLike == 1 ? "1 Like" : "\(post.numberOfLike.abbreviationNum()) Likes")"
            cell.numberOfCommentsLabel.text = "\(post.numberOfComment == 1 ? "1 comment" : "\(post.numberOfComment.abbreviationNum()) Comments")"
            
            cell.selectionStyle = .none
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ContentWithImageReuseIdentifier", for: indexPath) as! ContentWithImageTableViewCell
            cell.userSubLabel.text = "\(post.postDuration.abbreviationSeconds()) . \(post.location)"
            cell.userNameLabel.text = post.userName
            cell.profileImageView.image = UIImage(named: post.photoUrl)
            cell.postPhotoImageView.image = UIImage(named: post.postPhotoUrl!)
            cell.captionLabel.text = post.caption
            cell.postByPhotoImageView.image = UIImage(named: post.photoUrl)

            cell.numberOfLikesLabel.text = "\(post.numberOfLike == 1 ? "1 Like " : "\(post.numberOfLike.abbreviationNum()) Likes")"
            cell.numberOfCommentsLabel.text = "\(post.numberOfComment == 1 ? "1 Like" : "\(post.numberOfComment.abbreviationNum()) Comments")"
        

            cell.selectionStyle = .none
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if postDatas[indexPath.row].postPhotoUrl == nil {
                return 240
        }else {
                return 600
        }
    }
    
  
}
