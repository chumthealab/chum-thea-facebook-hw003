//
//  PostModel.swift
//  Chumthea-HomeWork-003
//
//  Created by MacBook on 12/3/19.
//  Copyright © 2019 MacBook. All rights reserved.
//

import Foundation

struct PostModel {
    var userName: String
    var photoUrl: String
    var postDuration: Int
    var location: String
    var caption: String
    var postPhotoUrl: String?
    var numberOfLike: Int
    var numberOfComment: Int
}

// MARK: - Custom Abbreviation Number for Post Duration
extension Int{
    func abbreviationSeconds() -> String{
        if self == 0 {
            return "Just Now"
        }else if self < 60 {
            if self == 1 {
                return "\(self) second ago"
            }
            return "\(self) seconds ago"
        }else if self < 60 * 60  {
            return "\(self / 60) \(self / 60 == 1 ? " minute" : " minutes") ago"
        }else if self < 60 * 60 * 24  {
            return "\((self / 60) / 60) \((self / 60) / 60 == 1 ? " hour" : " hours") ago"
        }else if self < 60 * 60 * 24 * 7 {
            return "\((self / 60) / 60 / 24) \(((self / 60) / 60 / 24) == 1 ? " day" : " days") ago"
        }else{
            return "\((((self / 60) / 60 ) / 24 ) / 7) \(((((self / 60) / 60 ) / 24 ) / 7) == 1 ? " week" : "weeks") ago"
        }
    }
}

// MARK: - Custom Abbreviation Number for like and comments
extension Int{
    func abbreviationNum() -> String{
        if self < 1000 {
            return "\(self)"
        }else if self < 1000000 {
            var n = Double(self)
            n = Double(floor(n/100)/10)
            return "\(n.description)K"
        }else {
            var n = Double(self)
            n = Double(floor(n/10000) / 100)
            return "\(n.description)M"
        }
    }
}

