//
//  FeedViewController.swift
//  Chumthea-HomeWork-003
//
//  Created by MacBook on 12/1/19.
//  Copyright © 2019 MacBook. All rights reserved.
//

import UIKit

class FeedTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .red
        
        tableView.register(UINib(nibName: "ContentOnlyTableViewCell", bundle: nil), forCellReuseIdentifier: "ContentOnlyTableViewCell")
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        3
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let contentOnlyCell = tableView.dequeueReusableCell(withIdentifier: "ContentOnlyTableViewCell") as! ContentOnlyTableViewCell
        contentOnlyCell.label.text = "Hello"
        contentOnlyCell.profileImageView.image = UIImage(systemName: "bell")
        return contentOnlyCell
    }
}
