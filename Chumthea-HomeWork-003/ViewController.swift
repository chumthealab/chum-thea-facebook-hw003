//
//  ViewController.swift
//  Chumthea-HomeWork-003
//
//  Created by MacBook on 12/1/19.
//  Copyright © 2019 MacBook. All rights reserved.
//

import UIKit

class ViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        customNavbar()
        
        addTabBarController()
        
    }


    func customNavbar(){
//        let logoImage = UIImage(named: "fb.logo")
        navigationController?.navigationBar.barTintColor = UIColor(red:0.26, green:0.40, blue:0.70, alpha:1.0)
        navigationItem.title = "Facebook"
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        navigationItem.largeTitleDisplayMode = .always
    }
    
    func addTabBarController(){
        let feedVC = FeedTableViewController()
        let vc1 = AnotherViewController()
        let vc2 = AnotherViewController()
        let vc3 = AnotherViewController()
        let vc4 = AnotherViewController()
                
        feedVC.tabBarItem.image = UIImage(systemName: "house.fill")
        vc2.tabBarItem.image = UIImage(systemName: "airplayvideo")
        vc4.tabBarItem.image = UIImage(systemName: "person.fill")
        vc3.tabBarItem.image = UIImage(systemName: "bell")
        vc1.tabBarItem.image = UIImage(systemName: "person.badge.plus.fill")
        let tabBarList = [feedVC, vc1, vc2, vc3, vc4]
        
        viewControllers = tabBarList
        
        
    }
}

